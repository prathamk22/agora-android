package org.aossie.agoraandroid.data.network.responses

import org.aossie.agoraandroid.data.db.model.VoterList

data class Voters(
  var voters: List<VoterList>
)


